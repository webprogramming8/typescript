type CarYear = number;
type CarType = String;
type CarModel =string;

type Car = {
    year: CarYear,
    type: CarType,
    model: CarModel,
}

const carYear: CarYear = 2001;
const carType: CarType = "Nissan";
const carModel: CarModel = "TENA";

const car1: Car = {
    year: carYear,
    type: carType,
    model: carModel,

}
console.log(car1);

